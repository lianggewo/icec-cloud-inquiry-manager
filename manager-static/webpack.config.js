const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackIncludeAssetsPlugin = require('html-webpack-include-assets-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const path = require('path');

const config = require('./config');

const devMode = config.env === 'development';

const cssLoaders = [
  'style-loader',  // : MiniCssExtractPlugin.loader, // creates style nodes from JS strings or extra css
  'css-loader', // translates CSS into CommonJS
  'postcss-loader',
];

// 将通用依赖拉出来，单独引入
const plugins = [
  new CopyWebpackPlugin([
    {
      from: 'node_modules/core-js/client',
      to: 'core-js/',
      cache: true
    },
    {
      from: 'node_modules/react/umd',
      to: 'react/',
      cache: true
    },
    {
      from: 'node_modules/react-dom/umd',
      to: 'react-dom/',
      cache: true
    },
    {
      from: 'node_modules/@casstime/bricks/dist',
      to: '@casstime/bricks',
      cache: true
    }
  ]),
  // new MiniCssExtractPlugin({
  //   // Options similar to the same options in webpackOptions.output
  //   // both options are optional
  //   filename: 'css/[name].[chunkhash:8].css',
  //   chunkFilename: 'css/[id].[chunkhash:8].css'
  // }),
  new HtmlWebpackPlugin({
    title: config.title,
    filename: 'index.html',
    template: 'index.html', // devMode ? 'index.dev.html' : 'index.html'
  }),
  new HtmlWebpackIncludeAssetsPlugin({
    assets: devMode ? [
      '@casstime/bricks/bricks.production.css',
      'core-js/core.min.js',
      'react/react.development.js',
      'react-dom/react-dom.development.js',
      '@casstime/bricks/bricks.production.js'
    ]
      : [
        '@casstime/bricks/bricks.production.css',
        'core-js/core.min.js',
        'react/react.production.min.js',
        'react-dom/react-dom.production.min.js',
        '@casstime/bricks/bricks.production.js'
      ],
    append: false,
    hash: true
  })
];

const optimization = {
  minimizer: [
    new UglifyJsPlugin({
      cache: true,
      parallel: true,
      sourceMap: true // set to true if you want JS source maps
    }),
  ],
  splitChunks: {
    chunks: 'all',
    minSize: 30000,
    maxSize: 0,
    minChunks: 1,
    maxAsyncRequests: 5,
    maxInitialRequests: 3,
    automaticNameDelimiter: '~',
    name: true,
    cacheGroups: {
      vendors: {
        test: /[\\/]node_modules[\\/]/,
        priority: -10
      },
      default: {
        minChunks: 2,
        priority: -20,
        reuseExistingChunk: true
      }
    }
  }
};

module.exports = {
  entry: './src/index.tsx',
  mode: devMode ? 'development' : 'production',
  devtool: devMode ? 'cheap-source-map' : 'hidden-source-map',
  output: {
    // 开发模式不用hash
    filename: devMode ? 'js/[name].js' : 'js/[name].[chunkhash:8].js',
    path: config.output,
    publicPath: config.publicPath
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json']
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader?cacheDirectory=true',
        exclude: /node_modules/,
        include: path.join(__dirname, 'src')
      },
      {
        test: /\.tsx?$/,
        use: ['babel-loader', 'ts-loader'],
        include: path.join(__dirname, 'src'),
        exclude: /node_modules/
      }, // 支持typescript
      {
        test: /\.scss$/, use: [...cssLoaders, 'sass-loader']
      },  // 支持 sass
      {
        test: /\.css$/, use: [...cssLoaders]
      }, // 支持 css
      {
        test: /(\.(eot|ttf|woff|woff2)|font\.svg)$/,
        loader: 'file-loader',
        options: { outputPath: 'fonts/' }
      },
      {
        test: /\.(jpe?g|png|svg|gif)$/,
        loader: 'file-loader',
        options: { outputPath: 'images' }
      } // 图片保存在images目录
    ]
  },
  plugins,
  optimization: devMode ? {} : optimization,
  externals: {
    'react': 'React',
    'react-dom': 'ReactDOM',
    '@casstime/bricks': 'bricks',
  },
  devServer: config.devServer
};
