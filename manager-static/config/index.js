const env = process.env.NODE_ENV || 'development';

const config = Object.assign({}, require('./base'), require(`./${env}`));
config.env = env;

module.exports = config;
