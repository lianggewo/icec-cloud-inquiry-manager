const path = require('path');

module.exports = {
  title: 'bricks-sample',
  output: path.resolve(__dirname, '../dist'),
  publicPath: '',
};
