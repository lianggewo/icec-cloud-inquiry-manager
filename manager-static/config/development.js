module.exports = {
  publicPath: '',
  devServer: {  // 参考 https://webpack.js.org/configuration/dev-server/
    proxy: {
      '/api': 'http://localhost:8000' // 实际请求到 http://localhost:8000/api
    }
  }
};
