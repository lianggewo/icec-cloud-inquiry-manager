import qs from 'query-string';

export const createLocationSearch = (query: object) => {
  return qs.stringify(query);
};

export const parseLocationSearch = (search: string) => {
  return qs.parse(search);
};
