import classNames from 'classnames';
import React from 'react';

export interface IMenuProps {
  className?: string;
  children?: React.ReactNode;
}

const Menu: React.SFC<IMenuProps> = (props) => {
  const className = classNames(['menu', props.className]);
  return <ul className={className}>{props.children}</ul>;
};

export default Menu;
