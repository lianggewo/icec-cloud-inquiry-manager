import React from 'react';

import Menu from './Menu';
import MenuItem from './MenuItem';

const Sidebar = () => {
  return (
    <Menu className="sidebar">
      <MenuItem to="/">主页</MenuItem>
      <MenuItem menu={
        <Menu>
          <MenuItem to="/test">test</MenuItem>
          <MenuItem to="/no-sidebar/test2">test2</MenuItem>
        </Menu>
      }>
        嵌套菜单
      </MenuItem>
    </Menu>
  );
};

export default Sidebar;
