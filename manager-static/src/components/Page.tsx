import { Grid } from '@casstime/bricks';
import React from 'react';

interface IPageProps {
  title?: string;
  children?: React.ReactNode
}

interface IPageState {
  error?: string | Error | null
}

export default class Page extends React.PureComponent<IPageProps, IPageState> {
  constructor(props: IPageProps) {
    super(props);
  }

  componentDidMount() {
    if (this.props.title) {
      document.title = this.props.title;
    }
  }

  render() {
    return (
      <div className="brs-page">
        <Grid className="brs-page__content" fluid>{this.props.children}</Grid>
      </div>
    );
  }
}
