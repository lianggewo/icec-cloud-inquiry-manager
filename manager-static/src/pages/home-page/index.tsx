import React from 'react';
import './styles/index.scss';

import { RouteConfigComponentProps } from 'react-router-config';
import Main from './components/Main';
import stores from './stores';
import { catchError } from '../../hoc';

const HomePage = (props: RouteConfigComponentProps<any>) => {
  return <Main {...stores} {...props}/>
}

export default catchError(HomePage);
