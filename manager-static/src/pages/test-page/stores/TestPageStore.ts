import { observable, action } from 'mobx';

class TestPageStore {
  @observable list = [
    { name: 'Jack', age: 28, address: 'some where', key: '1' },
    { name: 'Rose', age: 36, address: 'some where', key: '2' },
    { name: 'Jack', age: 28, address: 'some where', key: '3' },
    { name: 'Rose', age: 36, address: 'some where', key: '4' },
    { name: 'Jack', age: 28, address: 'some where', key: '5' },
    { name: 'Rose', age: 36, address: 'some where', key: '6' },
    { name: 'Rose', age: 36, address: 'some where', key: '7' },
    { name: 'Rose', age: 36, address: 'some where', key: '8' },
    { name: 'Rose', age: 36, address: 'some where', key: '9' },
    { name: 'Jack', age: 28, address: 'some where', key: '10' },
  ];

  @action
  removeListItem(index: number) {
    this.list.splice(index, 1);
  }

  @action
  changeName(index: number) {
    this.list[index].name = Math.random().toString(26).substr(2, 5);
  }
}

export default TestPageStore;
