import React from 'react';
import './styles/index.scss';

import { RouteConfigComponentProps } from 'react-router-config';
import Main from './components/Main';
import stores from './stores';
import { catchError } from '../../hoc';

const Test2Page = (props: RouteConfigComponentProps<any>) => {
  return <Main {...stores} {...props}/>
}

export default catchError(Test2Page);
