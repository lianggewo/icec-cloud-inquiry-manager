import React from 'react';
import { Toast } from '@casstime/bricks';

export default function catchError<T> (Com: React.ComponentType<T>) {
  return class extends React.Component<T, {}> {
    componentDidCatch(error: Error, info: React.ErrorInfo) {
      Toast.warn('页面出错了,请刷新页面后再试!', 2000);
      console.error('页面出错了', error, info);
    }

    render() {
      return <Com {...this.props} />
    }
  }
}
