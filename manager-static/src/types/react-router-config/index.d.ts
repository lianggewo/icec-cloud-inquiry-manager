import * as React from "react";
import { RouteComponentProps, SwitchProps, match } from "react-router";
import { Location } from "history";

export interface RouteConfigComponentProps<T> extends RouteComponentProps<T> {
  route?: RouteConfig;
}

export interface RouteConfig {
  location?: Location;
  component?: React.ComponentType<any>;
  path?: string;
  exact?: boolean;
  strict?: boolean;
  routes?: RouteConfig[];
}

export interface MatchedRoute<T> {
  route: RouteConfig;
  match: match<T>;
}

export function matchRoutes<T>(routes: RouteConfig[], pathname: string): Array<MatchedRoute<T>>;

export function renderRoutes(
  routes: RouteConfig[] | undefined,
  extraProps?: any,
  switchProps?: SwitchProps,
): JSX.Element;
