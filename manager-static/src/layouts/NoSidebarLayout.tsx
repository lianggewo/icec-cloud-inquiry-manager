import React from 'react';
import { renderRoutes, RouteConfigComponentProps } from 'react-router-config';

import Navbar from '../components/Navbar';

const NoSidebarLayout: React.SFC<RouteConfigComponentProps<void>> =  ({ route }) => {
  return (
    <div>
      <Navbar />
      <div>
        {route && renderRoutes(route.routes)}
      </div>
    </div>
  );
};

export default NoSidebarLayout;
