import React from 'react';
import { renderRoutes, RouteConfigComponentProps } from 'react-router-config';

import Navbar from '../components/Navbar';
import Sidebar from '../components/Sidebar';

const DefaultLayout: React.SFC<RouteConfigComponentProps<void>> =  ({ route }) => {
  return (
    <div>
      <Navbar />
      <div className="brs-main">
        <Sidebar />
        {route && renderRoutes(route.routes)}
      </div>
    </div>
  );
};

export default DefaultLayout;
