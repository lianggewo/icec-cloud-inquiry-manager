import React from 'react';
import Loadable from 'react-loadable';
import Loading from '../components/Loading';
import { RouteConfig } from 'react-router-config';

const routes: RouteConfig[] =  [
  {
    path: '/no-sidebar',
    component: Loadable({
      loader: () => import('../layouts/NoSidebarLayout'),
      loading: Loading
    }),
    routes: [
      {
        path: '/no-sidebar/test2',
        component: Loadable({
          loader: () => import('../pages/test2-page'),
          loading: Loading
        })
      }
    ]
  },
  {
    path: '/',
    component: Loadable({
      loader: () => import('../layouts/DefaultLayout'),
      loading: Loading
    }),
    routes: [
      {
        path: '/',
        exact: true,
        component: Loadable({
          loader: () => import('../pages/home-page'),
          loading: Loading
        })
      },
      {
        path: '/test',
        component: Loadable({
          loader: () => import('../pages/test-page'),
          loading: Loading
        })
      }
    ]
  }
];

export default routes;
