package com.casstime.inquiry.manager.api.config;/* *
 *
 *   @author  xuanfei.Chen
 *   @time   2018/10/22 16:04
 * */

import com.casstime.ec.cloud.config.GenericMybatisConfig;
import com.casstime.ec.cloud.values.inquiry.QuotedPrice;
import com.casstime.ec.cloud.values.inquiry.QuotedPriceItem;
import com.casstime.ec.cloud.values.inquiry.supplierList.SupplierList;
import com.casstime.ec.cloud.values.inquiry.supplierList.SupplierListGrouping;
import com.casstime.ec.cloud.values.inquiry.supplierList.SupplierListItem;
import com.casstime.ec.cloud.values.inquiry.supplierList.SupplierListItemStore;
import javax.sql.DataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class MybatisConfig extends GenericMybatisConfig {


  //使用时，需要指定name eg:@Transactional("defaultTransactionManager")
  @Bean(name="defaultTransactionManager")
  public PlatformTransactionManager defaultTransactionManager(DataSource dataSource) {
    DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(dataSource);
    transactionManager.setGlobalRollbackOnParticipationFailure(false);
    return transactionManager;
  }

  @Bean
  public SqlSessionFactory getSqlSessionFactory(DataSource dataSource) throws Exception {
    SqlSessionFactoryBean factoryBean = super.getSqlSessionFactoryBean(dataSource,
        "com.casstime.ec.cloud.inquiry.entity",
        "classpath:com/casstime/ec/cloud/inquiry/dao/**/*.xml");

    factoryBean.setTypeAliases(new Class[]{
        QuotedPrice.class,
        QuotedPriceItem.class,
        SupplierListGrouping.class,
        SupplierList.class,
        SupplierListItem.class,
        SupplierListItemStore.class});

    return factoryBean.getObject();
  }

  @Bean
  public MapperScannerConfigurer getMapperScannerConfigurer() {
    return super.getMapperScannerConfigurer("getSqlSessionFactory", "com.casstime.ec.cloud.inquiry.dao");
  }

}
